import json
from django.test import Client  # like Postman
from django.urls import reverse
import pytest
from companies.models import Company

####################
import os
from pathlib import Path
BASE_DIR = Path(__file__).resolve().parent.parent

companies_url = reverse("companies-list")
pytestmark = pytest.mark.django_db


'''
django_db is our test db and it is the replica of our prod db.
'''


# ----------------Test Get Companies -------------------------

def test_zero_companies_should_return_empty_list(client) -> None:
    response = client.get(companies_url)  # client yani postman sends a get request
    assert (response.status_code == 200)  # cevap bos gelecek ama sorgu yapildi bos liste geldi cunku company yok!
    assert (json.loads(response.content) == [])  # company provision edilmemis o yuzden bos liste gelecek, test passed olacak ama jason formatinda olacagi icin
    # response code json.loads yazilacak!

def test_one_company_exits_should_succeed(client) -> None:
    test_company = Company.objects.create(name="Amazon")
    print(f"companies uls are --------- {companies_url}")
    response = client.get(companies_url)
    print(f"response : -------------------------- {json.loads(response.content)[0]}")
    response_content = json.loads(response.content)[0]
    assert (response.status_code == 200)
    assert (response_content.get("name") == test_company.name)
    print(f"Company name ---------------------------------- {test_company.name}")


# ----------------Test POST Companies -------------------------



def test_create_company_without_argument_should_fail(client) -> None:
    response = client.post(path=companies_url)
    assert (response.status_code == 400)
    assert (json.loads(response.content) == {"name": ["This field is required."]})



def test_create_existing_company_should_fail(client) -> None:
    Company.objects.create(name="apple")
    response = client.post(path=companies_url, data={"name": "apple"})
    assert response.status_code == 400
    assert (json.loads(response.content) == {"name": ["company with this name already exists."]})



def test_create_company_with_only_name_all_fields_all_fields_should_be_default(client) -> None:
    response = client.post(path=companies_url, data={"name": "test company name"})
    assert response.status_code == 201
    response_content = json.loads(response.content)
    print(f" -------------------------------------------{response_content}")
    assert (response_content.get("application_link") == "")
    assert (response_content.get("status") == "Hiring")
    assert (response_content.get("notes") == "")


@pytest.mark.xfail
def test_should_be_ok_if_fails(self) -> None:
    self.assertEqual(1, 2)

##########################################################################################################


def raise_exception() -> None:
    raise ValueError("CoronaVirus Exception")


def test_raise_exception_should_pass() -> None:
     with pytest.raises(ValueError) as e:  # This means we are raising a value error in this test
        raise_exception()
     assert "CoronaVirus Exception" == str(e.value)


##########################################################################################################

import logging

logger = logging.getLogger('CORONA_LOGS')


def function_that_logs_something() -> None:
    try:
        raise ValueError("CoronaVirus Exception")
    except ValueError as e:
        logger.warning(f"I am logging {str(e)}")


def test_logged_warning_level(caplog) -> None:
    function_that_logs_something()
    assert "I am logging CoronaVirus Exception" in caplog.text




