# Pytest
## Pytest Feature
- Fixture : Add functionality to our test
- Markers : tag tests, test filtering
- Parametrize : run the same test again and again  with multiple inputs
- Skip : skip the test
- xfail: it is ok that this test is fail
- pytest.ini : configuration
- Pytest-django
- Pytest-cov : how much are code is covered with our test
- pytest-xdist : parallel test 
- unittest library, mocks : standart test library


We gave a name starting with test_ because pytest discovers that this is a test file. This is configurable.
```bash
def test_our_first_test() --> None
	assert 1==2
	
cd /directory
pipenv shell
pytest .
or 
pytest /testdirectory/test_1.py -v 
```

how to run by supressing the warnings
```
pytest /testdirectory/test_1.py -v -p no:warnings
```

test edilen function return degerini print etmesi icin -s kullanilir.
```
pytest /testdirectory/test_1.py -v -p no:warnings -s
```

this will run only the test marked as slow.
```
pytest /testdirectory/test_1.py -v -p no:warnings -m slow
```

this will run only the test which are not marked as slow.
```
pytest /testdirectory/test_1.py -v -p no:warnings -m "not slow"
```

This will run always because 4> 2
```
@pytest.mark.skipif(4>2, reason="sBecause 4>2")
def test_our_first_test() --> None
	assert 1==2
```

Does not impact the report. Result is going to be xfail or xpass
```
@pytest.mark.xfail
def .....
```

Parametrize (run these tests with -s to see what the return value is)
Test is going to be executed 3 times for each element in the list.
```
@pytest.mark.parametrize("company_name",["c1","c2","c3"])
def testparametrized(company_name) -> None
	print(f"Test {company_name}")
```

# pipenv 
pipenv is our packeage manager. manage dependencies

install pipenv
```bash
brew pipenv install
```

Start the virtual environment
```bash
pipenv shell
```

Install pytest into the virtual enviromnent
```bash
pipenv install pytest
```

# Django Application 

In the further steps we will develop a RestAPI with the Rest Framework so we first do the installation.

```
pipenv install djangorestframework
```
You have to add your new app "rest_framework", into the installed apps in settings.py.

We have to create a folder for our new project which is called api.

```
mkdir api
```

We have to go to our project directory and we start a project there.

```
cd api

django-admin startproject djangoPytest
```

We create our database and migrate our database. Otherwise we will receive no such table error.
```
$ python manage.py migrate
```

After we created the database we can create a user for the admin panel.
```
python manage.py createsuperuser
```

We go into the project directory and started our web server.
```
cd djangoPytest

python manage.py runserver
```

We can now create a new django application/service. 
As this is a new application, we have to put into the settings.py (INSTALLED_APS).
```
python manage.py startapp companies
```

What is next?
- We will create our model.

# Model
Model class is going to be created in models.py. Django already have models library.
Your model class will have atributes like company_name, company_status etc...
Your application is going to be good as much as your model attributes are rich.
CharField, URLField, TextChoices are some ready to use functions o Django models library.

- if not done yet, we will add our application name into installed apps and also including the rest-framework too.

# Database Migrations
- We will make migration to change the database schema as we created a model or made a change in the model.
```
python manage.py makemigration companies
```
- After makemigrations  command is executed a '0000x_init.py' migration script is going to be generated. \
This .py file contains the tables for our new application model.Below command is going to insert the new database schemas. 
```
python manage.py migrate
```
# Register Model to Django Admin
- We will register our application object into django admin (admin.py). We add the below code into admin.py.
```
@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
	pass
```

Now we can see our model in the admin panel. We can do personelization on our admin GUI. \
But we do not do this for this project.


# Install black(optional)
Python style formatter.
```
$ pipenv install --dev black==19.3b0
```

# MVC Model	
					
```text
							  __ Model
							 |
							 |
User---Django---URL---View --|								
							 |  
							 |__ Template
							

```

# Creating RestAPI with Django (Rest framework)
- First we create a file called serializers.py.
- Seriliazier takes our model from database and converts this into JSON format.
- We only mention in seriliazer.py what model we want to seriliaze and which fields do we want.

# View
We will develop the functions in views.py to execute the POST, GET Requests for our model.
We need to specify two things for an API. Optionally three including pagination.
- What is the serializer?
- Your queryset(Basicly ORM commands in otherwords what do you want to return).

# URLs 
URLs are our controllers. This is basicly configuring your routes. \
We will specify the routes that our views are going to use.
- We will create a new file under our application called urls.py. 
- We create a router for our URL. It is going to be DefaultRouter which is a class from Django.
- Then we will register a route called "companies". So it wlll become "http:127.0.0.1:8000/companies"

Note1: This route will point the function which we created in the views.py.
Note2: In the URL mapping we gave a basename for the URL. This we be very easy when we develop the test cases.

Next Step is to take the URLs we created for our application and we need to apply them for our entire django project.
'In the project/urls.py we will add all the urls that we created in our application/urls.py'
- To do that we want to take the root of our application which is "" and include(our urls) that we created.
```
path("",include(companies_router.urls))
```
# Testing with Postman
The cool thing with django is the dynamic url will be ready by it self.

- http://127.0.0.1:8000/companies/7/ is going to return the company with id=7
- http://127.0.0.1:8000/companies/ is going to return all companies.
- http://127.0.0.1:8000/ is the application where you can create company.


# Write Automated tests with python
First thing is to create a test directory inside our application.

- python manager.py runserver
- pytest-django should be installed because test db should be created for our django tests. 
- pytest-django is an extention for testing django and it is used as a decorater for the test.
```
pipenv install django-db
```
- After installation, you have to create pytest.ini and add DJANGO_SETTINGS_MODULE = djangoPytest.settings
- when you run your test,  Be sure that you are in the application directory.
- Client() is a class from django test module "from django.test import Client" 
- client sends get/post request like postman.
- "companies" application is the basename and reverse('companies-list') url adresi bir variable atamis.

- Below statement is the same with companies_url = "http:127.0.0.1:8000/companies/". This is not comman practice so...:
```
companies_url = reverse("companies-list") 
```

- You can send get Request to URL as below. You will retrive all data as an object.
```
response = client.get(companies_url)  
``` 
   
- You can get the content as below.
```
print(response.content) 
```

- Testi -s option ile kosarsan print statementi gosterir.
```
pytest -v -s
```

- First element from the get request. 
```
response_content = json.loads(response.content)[0] 
```

- name attirubute of the content is being compared with string "Amazon"
```
self.assertEqual(response_content.get("name"), "Amazon")
```

# setUp() and tearDown() Functions 
These are special functions of python testing.\ 
They run before each test! These are fixtures in pytest called client which is given as argumant into tests. 

# assertIn
Looks first given string is not in the response. For example:
```
self.assertIn("Is not a valid choice", str(response.content))
```

# xail
A test fail but you know the reason maybe becasue of a bug which is going to be fixed soon. So you do not want to delete 
the test. It is a decrater. 

```
@pytest.mark.xfail
```

Result : 5 passed, 1 xfailed 

# Time Tracking

```bash
pytest -v -s --durations=0
```

# Pytest exceptions
You can use "with pytest.raises(ValueError) as e:" to create an exception. 

```bash
def test_raise_exception_should_pass() -> None:
     with pytest.raises(ValueError) as e:  			# This means we are raising a value error in this test
        raise_exception()   						            # This part is not important, it can be becasue anything that the valueerror is caused
     assert "CoronaVirus Exception" == str(e.value)
```

# Pytest that assert Logs
```
import logging 

logger = logging.getLogger('CORONA_LOGS')

def function_that_logs_something() -> None:
	try:
		raise ValueError("CoronaVirus Exception")
	except ValueError as e:
		logger.warning(f"I am logging {str(e)}")
```	
# Test runner
-v : verbose
-s : print statemenlar
-k: keyword ile olalari calistir.

- Runs the test which comtains 'create in te naming.
```
pytest -v -s -k "create"
```

- Below includes test but not zero!!
```
pytest -v -s -k "test and not zero"
```

- Run the test which have specific markers
```bash
pytest -v -s --durations=0 -m xfail
```

# Testing Django Applications
pytest-django is an extansion for pytest to test django applications.

```bash
pipenv install pytest-django
```
- @pytest.mark.django_db
- Client : Post ans Get requests

# Continous Integration
- Bitbucket pipelines are used.
- yml file is used  with python image.
- Environment variables are used in yml file but these can also be done in repository settigs -- Reposiroy Variables.

# Django Email Service

We want to configure an email and an endpoint (http://127.0.0.1:8000/send-email).
Everytime we POST this endpoint to take the payload and send to the email.

In order to do that we have to change only 3 files:  views.py, urls.py and settings.py.


- In the views.py
Below is needed to send the Post so It is needed by the endpoint.
```
from rest_framework.decorators import api_view

@api_view(http_method_names=["POST"])     # Basically means threats this function as a POST endpoint!
send_company_email()
```
Email sending : https://docs.djangoproject.com/en/4.0/topics/email/

- settings.py
Outlook credentials etc.. is ging to be added.

- In the urls.py
We define send-email url extension which is going to trigger send_company_email() function.\

```
path("send-email", send_company_email), 
```

"ozencemailtest@gmail.com"














